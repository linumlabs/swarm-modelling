{ pkgs ? import <nixpkgs> { }, stdenv ? pkgs.stdenv, fetchPypi ? pkgs.fetchPypi
, pythonPkgs ? pkgs.python37Packages
, buildPythonPackage ? pythonPkgs.buildPythonPackage }:

buildPythonPackage rec {
  pname = "cadCAD";
  version = "0.4.15";

  src = builtins.fetchGit {
    url = "https://github.com/cadCAD-org/cadCAD.git";
    ref = "master";
  };

  doCheck = false;

  buildInputs = [ ];
  checkInputs = [ ];
  propagatedBuildInputs = with pythonPkgs; [
    numpy
    pandas
    pathos
    fn
    tabulate
    funcy
  ];

  meta = with stdenv.lib; {
    description =
      "Design, test and validate complex systems through simulation in Python";
    homepage = "https://github.com/BlockScience/cadCAD";
    license = licenses.mit;
    # maintainers = with maintainers; [ benschza ];
    platforms = platforms.unix;
  };
}
