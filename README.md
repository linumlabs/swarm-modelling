# Swarm Bonding Curve Modelling

* `main.ipynb` - main Jupyter Notebook model
* `notebooks/` - assorted workspace notebooks
* `model/` - various model helper functions

## Installing [Nix](https://nixos.org/) on Linux or macOS

```bash
sh <(curl https://nixos.org/nix/install)
```

## Dependencies

See `default.nix` - `iPython.packages` for a list of Python dependencies managed by Nix.

Optionally, you can also install additional depedencies using virtualenv in project root `venv/` directory:

```bash
nix-shell
virtualenv venv
source venv/bin/activate
python -m pip install _
```

and import the virtualenv dependencies by adding this to the top of the Notebook:

```
import sys
sys.path.append("./venv/lib/python3.8/site-packages")
```

## Development

`./start-jupyter.nix`

**OR**

```bash
nix-shell -i bash --pure --option sandbox false
$ jupyter lab --allow-root --no-browser --ip=0.0.0.0 --port=8888 --LabApp.token=''
```

## Generate main.pdf

`nix-build -A pdf`
