with import <nixpkgs> { };

let
  jupyter = import (builtins.fetchGit {
    url = "https://github.com/tweag/jupyterWith";
  }) { };

  iPython = jupyter.kernels.iPythonWith {
    name = "jupyterlab-swarm";
    packages = p:
      let cadCAD = p.callPackage ./cadCAD/default.nix { };
      in with p; [
        cadCAD
        black
        matplotlib
        seaborn
        scipy
        networkx
        ipywidgets
        mpmath
        sympy
        # Included in cadCAD package:
        # numpy
        # pandas
        # pathos
        # fn
        # tabulate
        # funcy
      ];
  };

  pdf = stdenv.mkDerivation {
    name = "pandoc-pdf";
    src = ./.;
    buildInputs = [
      pandoc
      (texlive.combine {
        inherit (texlive) scheme-full collection-basic collection-latex;
      })
    ];

    FONTCONFIG_FILE = makeFontsConf { fontDirectories = [ lmodern ]; };

    buildPhase = ''
      pandoc --pdf-engine=xelatex -V geometry:margin=2cm main.ipynb -o main.pdf
    '';

    installPhase = ''
      mkdir -p $out
      cp main.pdf $out
    '';
  };
in {
  env = (jupyter.jupyterlabWith {
    kernels = [ iPython ];
    extraPackages = p: with p; [
      python38
      python38Packages.virtualenvwrapper
      python38Packages.pip
    ];
    directory = jupyter.mkDirectoryWith {
      extensions = [ "@jupyter-widgets/jupyterlab-manager" ];
    };
  }).env;
  pdf = pdf;
}
